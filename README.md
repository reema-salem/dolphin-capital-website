# Dolphin-Capital-Website

A responsive website developed end-to-end for a client using Bootstrap and HTML, hosted as a one-page static website on AWS S3 and CloudFront CDN.

## Description

I developed a responsive website end-to-end for a client using **Bootstrap** and **HTML**. The website is designed to be a one-page static site, and it is hosted on AWS S3 with CloudFront CDN for improved performance and caching.

## Challenges 

I encountered challenges optimizing this website for mobile device use. These configurations performed flawlessly on desktop but when accessed from mobile platforms, the sizing and image placement were misaligned, despite my attempts to adjust the dimensions and rewrite the code. However, I successfully resolved the problem by substituting the problematic image and adjusting the background-size, this code npw serves as dual-performing website.

## Deployment

This website is deployed using **Terraform**. Please see my repo for those configurations here -> [Terraform SPA Deployment Repo](https://gitlab.com/reema-salem/terraform-single-page-application)